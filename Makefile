CC = $(CROSS_COMPILE)gcc
CFLAGS = -O2 -Wall
CFLAGS += -I/usr/include/libnl3 -I/usr/include -Wno-address-of-packed-member

ifdef NLROOT
CFLAGS += -I${NLROOT}
endif

LDFLAGS += -L/lib
LIBS += -lm

CFLAGS+=-DCONFIG_LIBNL32
LIBS += -lnl-3 -lnl-genl-3

OBJS = nvs.o misc_cmds.o calibrator.o plt.o wl18xx_plt.o ini.o

%.o: %.c calibrator.h nl80211.h plt.h nvs_dual_band.h
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(OBJS) 
	$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o calibrator

uim:
	$(CC) $(CFLAGS) $(LDFLAGS) uim_rfkill/$@.c -o $@

static: $(OBJS) 
	$(CC) $(LDFLAGS) --static $(OBJS) $(LIBS) -o calibrator

install:
	@echo Copy files to /usr/bin
	@cp -f ./calibrator /usr/bin
	@chmod 755 /usr/bin/calibrator

clean:
	@rm -f *.o calibrator uim
